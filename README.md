# DevOps Cloud Week

- Instrutor: Leandro Porciuncula e João Vitor Sell
- Plataforma de Ensino: [Canal Youtube - Cloud Treinamentos](https://www.youtube.com/channel/UCp3V_2F4l8_2moPPmSx6vng)
- Projeto para a prática da Semana DevOps Cloud Week da Cloud Treinamentos

- [Dia 1](https://youtu.be/z6PIkqXOwj4)

<hr>

## Autor

- Bruno Gamba Rocha
- [https://www.linkedin.com/in/bruno-gamba-rocha](https://www.linkedin.com/in/bruno-gamba-rocha/)



<hr>

## Tecnologias Utilizadas

- [Git](https://git-scm.com/)
- [VS Code](https://visualstudio.microsoft.com/pt-br/)
- [AWS Cloud](https://aws.amazon.com/pt/)
- [GitLab](https://gitlab.com/)
- [Docker Hub](https://hub.docker.com/)
- [Jenkins](https://www.jenkins.io/)



<hr>


### Application
A simple ReactJS app that displays latest stats about COVID-19.

### Installation
- git clone https://github.com/sambreen27/covid19.git
- cd covid19
- npm install
- npm start

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### COVID-19 API

GET https://covid19.mathdro.id/api -> global cases information

GET https://covid19.mathdro.id/api/countries -> countries that are available in the API

GET https://covid19.mathdro.id/api/countries/{countryName} -> get specific country's covid-19 cases data

API provided by: https://github.com/mathdroid/covid-19-api


### Tech Stack

- [React JS](https://github.com/facebook/react)
- [Axios](https://github.com/axios/axios)
- [Charts.js](https://github.com/chartjs/Chart.js)
- [material-ui](https://github.com/mui-org/material-ui)
- [react-particles-js](https://github.com/Wufe/react-particles-js)
- [react-countup](https://github.com/glennreyes/react-countup)

### Preview

- ![Global Data](https://github.com/sambreen27/covid19/blob/master/src/images/covid1.png) 
![Global Data](https://github.com/sambreen27/covid19/blob/master/src/images/covid2.png) 
![Specific Country Data](https://github.com/sambreen27/covid19/blob/master/src/images/covid3.png)